/* 
 * Copyright 2014 Roman Leonenkov <contact@leonenkov.ru>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.leonenkov.swingcomponents.databasemenu;

import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.event.ActionListener;

public class DatabaseMenu extends JPanel {

    public DatabaseMenu() {
        initComponents();
    }

    public void onCreateButtonPress(ActionListener l) {
        btnCreate.addActionListener(l);
    }

    public void onUpdateButtonPress(ActionListener l) {
        btnUpdate.addActionListener(l);
    }

    public void onDeleteButtonPress(ActionListener l) {
        btnDelete.addActionListener(l);
    }

    public void setCreateButtonEnabled(boolean b) {
        btnCreate.setEnabled(b);
    }

    public void setUpdateButtonEnabled(boolean b) {
        btnUpdate.setEnabled(b);
    }

    public void setDeleteButtonEnabled(boolean b) {
        btnDelete.setEnabled(b);
    }

    public void setButtonsEnabled(boolean b) {
        setCreateButtonEnabled(b);
        setUpdateButtonEnabled(b);
        setDeleteButtonEnabled(b);
    }

    private void initComponents() {

        btnCreate = new JButton();
        btnUpdate = new JButton();
        btnDelete = new JButton();

        btnCreate.setIcon(new ImageIcon(getClass().getResource("/images/create.png")));
        btnUpdate.setIcon(new ImageIcon(getClass().getResource("/images/update.png")));
        btnDelete.setIcon(new ImageIcon(getClass().getResource("/images/delete.png")));

        MigLayout layout = new MigLayout();
        this.setLayout(layout);
        this.add(btnCreate);
        this.add(btnUpdate);
        this.add(btnDelete);

//        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
//        this.setLayout(layout);
//        layout.setHorizontalGroup(
//                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
//                        .addGroup(layout.createSequentialGroup()
//                                .addComponent(btnCreate, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
//                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
//                                .addComponent(btnUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
//                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
//                                .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
//        );
//        layout.setVerticalGroup(
//                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
//                        .addComponent(btnCreate, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
//                        .addComponent(btnUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
//                        .addComponent(btnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
//        );
    }

    private JButton btnCreate;
    private JButton btnDelete;
    private JButton btnUpdate;
}
